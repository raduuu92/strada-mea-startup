let express = require('express');
let app = express();
let mongoose = require('mongoose');
let bodyParser = require('body-parser');
let api = require('./api/api');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://127.0.0.1/strada-mea', { useMongoClient: true });

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use('/api', api);

let port = 3000;
app.listen(port, function() {
    console.log("port is : ", port);
})

// aceeasi chestie si pentru contacte si sa aiba
// email, fullname, adresa, DOB, telefon, userul care a creeat