///// creeaza variabila router

let router = require('express').Router();
// let userMid = require('./middleware/userMiddleware');

// router.use('/contacts', require('./contact/contactRoutes'));

/////pe ruta /users face apel la /routes/user routes

router.use('/users', require('./user/userRoutes')); 

/////exporta variabila router dupa ce au fost facute call-urile
module.exports = router;