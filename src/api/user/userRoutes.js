// face apelul la router si la user controller
let controller = require('./userController');
let router = require('express').Router();

//pe ruta /users/ apeleaza post din controller
router.route('/').post(controller.post);

//exporta router dupa ce au fost facute callurile
module.exports = router;