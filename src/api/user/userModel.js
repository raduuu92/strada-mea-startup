//se apeleaza mongoose 
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

// creeaza o noua schema/model pentru user
let UserSchema = new Schema({
    fullName: {
        type: String,
        required: true
    },

    username: {
        type: String,
        required: true,
        unique: true
    },

    email: {
        type: String,
        required: true,
        unique: true
    },

    password: {
        type: String,
        required: true
    },

    role: {
        type: String,
        required: true
    },

    archived: Boolean,
    
    createdAt: Date,
    
    updatedAt: Date
});
//exporta schema si modelul
module.exports = mongoose.model('User', UserSchema);