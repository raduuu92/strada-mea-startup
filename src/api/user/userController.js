// apeleaza modelul
let User = require('./userModel');

// primeste datele de pe body si le trimite mai departe spre model
exports.post = function(req, res, next) {
    console.log(req.body);
    let newUser = new User(req.body);

    newUser.createdAt = new Date();
    newUser.updatedAt = new Date();

//se afiseaza obiectul dupa ce a fost bagat in baza de date
    newUser.save(function(usr) {
        User.find(usr)
            .select('-password')
            .exec()
            .then(function(usr) {
                res.json(usr);
            })
    });
};

