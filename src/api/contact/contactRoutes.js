let controller = require('/contactController');
let midd = require('./contactMiddleware');
let router = require('express').Router();

router.route('/')
    .post(/*midd.verifyCertificates(), midd.verifyOtherLanguages(), midd.verifyHasDriven(), */ midd.addOwner(), controller.post);

module.exports = router;