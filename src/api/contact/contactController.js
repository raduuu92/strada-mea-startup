let Contact = require('./contactModel');

exports.post = function(req, res, next) {
    let newContact = new Contact(req.body);

    newContact.createdAt = new Date();
    newContact.updateAt = new Date();

    newContact.save(function(err, contact) {
        contact
            .execPopulate().then(function(contact) {
                res.json(contact);
            });
    });
};
