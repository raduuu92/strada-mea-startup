var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
let _ = require('lodash');

exports.addOwner = function() {
    return function(req, res, next) {
        req.body.contactOwner = req.LoggedInUser._id + '';
        next();
    };
};