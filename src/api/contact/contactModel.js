let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let contactSchema = new Schema({

    email : {
        type: String,
        required: true
    },

    fullName: {
        type: String,
        required: true
    },

    adress: {
        type: String,
        required: true
    },

    DOB: {
        type: Date,
        required: true
    },

    phoneNo: {
        type: String,
        required: true
    },

    createdAt: Date,
    
    updatedAt: Date
});

module.exports = mongoose.model('contact', ContactSchema);